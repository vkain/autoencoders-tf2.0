import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib
import numpy as np
import os


def plot_VAE(model, test_dataset, img_shape):
    if not os.path.exists("results"):
        os.makedirs("results")
    # --- Reconstruction plot ---

    n = 5
    sample_dataset = test_dataset
    x_input, y_input = next(sample_dataset.__iter__())
    x_input_sample, y_input_sample = map(lambda x: x[:n], (x_input, y_input))
    z = model.encode(x_input_sample)[0].numpy()
    z_dim = z.shape[1]

    fig, axarr = plt.subplots(2, 5, figsize=(20, 8))
    x_input_sample = x_input_sample.numpy().reshape([n, img_shape[0], img_shape[1]])
    x_output = model.decode(z, apply_sigmoid=True).numpy().reshape([n, img_shape[0], img_shape[1]])

    for i in range(n):
        axarr[0, i].axis('off')
        axarr[1, i].axis('off')
        axarr[0, i].imshow(x_input_sample[i], cmap='Greys_r')
        axarr[1, i].imshow(x_output[i], cmap='Greys_r')

    fig.savefig("results/VAE_reconstruction.png")

    '''
    These parts are really significant only if the latent dimension is 2,
    but work in other cases anyway.
    '''

    # --- Distribution plot ---
    z, _ = model.encode(x_input)
    z1, z2 = z.numpy().T[0], z.numpy().T[1]
    fig_dist = plt.figure(figsize=(8, 8))
    ax_dist = fig_dist.add_subplot(111)
    ax_dist.scatter(z1, z2)
    fig_dist.savefig("results/VAE_distribution.png")

    # --- Conceptual plot ---
    n = 5
    f, axarr = plt.subplots(n, n, figsize=(8, 8))
    f.subplots_adjust(hspace=0., wspace=-0.)

    zs = np.random.normal(size=(z_dim))

    for i, z1 in enumerate(np.linspace(-2, 2, n)):
        for j, z2 in enumerate(np.linspace(-2, 2, n)):
            zs[0] = z1; zs[1] = z2
            z = np.array([zs])
            generated_img = model.decode(z, apply_sigmoid=True).numpy().reshape([img_shape[0], img_shape[1]])
            axarr[i, j].axis('off')
            axarr[i, j].imshow(generated_img, cmap='Greys_r')
    f.savefig("results/VAE_conceptual.png")

