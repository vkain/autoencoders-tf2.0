# parser_args code referred the hwalseoklee's code:
# https://github.com/hwalsuklee/tensorflow-mnist-VAE/blob/master/run_main.py
import tensorflow as tf
from utils import loaders, plot
from model.autoencoder import VAE
from train_utils.autoencodertrain import VAETrain
import time
import argparse


def parse_args():
    desc = "Tensorflow 2.0 implementation of 'AutoEncoder Families (AE, VAE, CVAE(Conditional VAE))'"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument('--z_dim', type=int, default=2,
                        help='Degree of latent dimension(a.k.a. "z")')
    parser.add_argument('--epochs', type=int, default=100,
                        help='The number of training epochs')
    parser.add_argument('--lr', type=float, default=1e-4,
                        help='Learning rate during training')
    parser.add_argument('--batch_size', type=int, default=1000,
                        help='Batch size')
    parser.add_argument('--img_src', type=str, default='/gdrive/My Drive/schottky/PNG/original',
                        help='directory containing original images')
    parser.add_argument('--dropout', type=float, default=0.2,
                        help='dropout rate (0 for off)')
    return parser.parse_args()


def train_VAE(z_dim = 2,
              img_shape = (256, 256),
              epochs=100,
              lr=1e-4,
              batch_size=1000,
              dropout = 0.2,
              img_src = '/gdrive/My Drive/schottky/PNG/original'):

    model = VAE(z_dim, img_shape, dropout)


    #train_dataset, test_dataset = loaders.load_dataset(batch_size=batch_size)

    train_dataset, test_dataset = loaders.load_schottky_dataset(img_src, img_shape, batch_size=batch_size, )

    optimizer = tf.keras.optimizers.Adam(lr)

    for epoch in range(1, epochs + 1):
        t = time.time()
        last_loss = 0
        for train_x, _ in train_dataset:
            gradients, loss = VAETrain.compute_gradients(model, train_x)
            VAETrain.apply_gradients(optimizer, gradients, model.trainable_variables)
            last_loss = loss
        if epoch % 10 == 0:
            print('Epoch {}, Loss: {}, Remaining Time at This Epoch: {:.2f}'.format(
                epoch, last_loss, time.time() - t
            ))

    plot.plot_VAE(model, test_dataset, img_shape)

    return model


def main(args):

    train_VAE(
        z_dim=args.z_dim,
        epochs=args.epochs,
        lr=args.lr,
        batch_size=args.batch_size,
        img_src = args.img_src,
        dropout = args.dropout
    )


if __name__ == "__main__":
    args = parse_args()
    # if args is None:
    #     exit()

    main(args)
