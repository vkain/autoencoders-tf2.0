#dev version for Schottky
import tensorflow as tf
from tensorflow.keras.layers import InputLayer, Conv2D, Flatten, Dense, Dropout, Conv2DTranspose, Reshape, LeakyReLU

class VAE(tf.keras.Model):
    def __init__(self, latent_dim, img_shape, dropout):
        super(VAE, self).__init__()
        self.latent_dim = latent_dim
        self.img_shape = img_shape

        self.inference_net = tf.keras.Sequential([
            InputLayer(input_shape=[self.img_shape[0], self.img_shape[1], 1]),
            Conv2D(filters=32, kernel_size=3, strides=(2, 2), activation='relu'),
            Dropout(rate=dropout),
            Conv2D(filters=64, kernel_size=3, strides=(2, 2), activation='relu'),
            Dropout(rate=dropout),
            Conv2D(filters=128, kernel_size=3, strides=(2, 2), activation='relu'),
            Dropout(rate=dropout),
            Conv2D(filters=256, kernel_size=3, strides=(2, 2), activation='relu'),
            Dropout(rate=dropout),
            Conv2D(filters=512, kernel_size=3, strides=(2, 2), activation='relu'),
            Dropout(rate=dropout),
            Flatten(),
            Dense(256, activation='relu'),
            # No activation
            Dense(self.latent_dim * 2),  # [means, stds]
        ])

        self.generative_net = tf.keras.Sequential([
            InputLayer(input_shape=[self.latent_dim]),
            Dense(256, activation='relu'),
            Dropout(rate=dropout),
            Dense(8 * 8 * 32, activation='relu'),
            Reshape(target_shape=(8, 8, 32)),
            Dropout(rate=dropout),
            Conv2DTranspose(filters=512, kernel_size=3, strides=(2, 2), padding="SAME", activation='relu'),
            Dropout(rate=dropout),
            Conv2DTranspose(filters=256, kernel_size=3, strides=(2, 2), padding="SAME", activation='relu'),
            Dropout(rate=dropout),
            Conv2DTranspose(filters=128, kernel_size=3, strides=(2, 2), padding="SAME", activation='relu'),
            Dropout(rate=dropout),
            Conv2DTranspose(filters=64,kernel_size=3,strides=(2, 2),padding="SAME",activation='relu'),
            Dropout(rate=dropout),
            Conv2DTranspose(filters=32, kernel_size=3, strides=(2, 2), padding="SAME", activation='relu'),
            Dropout(rate=dropout),
            # No activation
            Conv2DTranspose(filters=1, kernel_size=3, strides=(1, 1), padding="SAME"),
        ])

    def encode(self, x):
        mean_logvar = self.inference_net(x)
        N = mean_logvar.shape[0]
        mean = tf.slice(mean_logvar, [0, 0], [N, self.latent_dim])
        logvar = tf.slice(mean_logvar, [0, self.latent_dim], [N, self.latent_dim])
        return mean, logvar

    def decode(self, z, apply_sigmoid=False):
        logits = self.generative_net(z)
        if apply_sigmoid:
            probs = tf.sigmoid(logits)
            return probs
        return logits

    def reparameterize(self, mean, logvar):
        eps = tf.random.normal(shape=mean.shape)
        return eps * tf.exp(logvar * .5) + mean
