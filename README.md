# Auto-Encoders for Schotty spectra using TF 2.0
Implementations of AE(Auto-Encoder), VAE(Variational AE) and CVAE(Conditional VAE)

## Results

### VAE(Variational Auto-Encoder)

```
python run_main.py --ae_type VAE --num_epochs 150 --latent_dim 2
```

<table>
<tr>
<td>Original (top) and Reconsructed Images</td>
<td><img src='results/VAE_reconstruction.png'></td>
</tr>
<tr>
<td>Distribution of z</td>
<td><img src='results/VAE_distribution.png' height=500></td>
</tr>
</table>


## More Conceptual Plots

### VAE

Visualizations of generated images from (z1, z2) ∈ [-2, 2] uniform distribution (other z's are random.normal).

![](results/VAE_conceptual.png)

## Usage

### Prerequisites

1. `tensorflow 2.0.0-alpha0` or higher
2. python packages: `numpy`, `matplotlib`

### Command

```
python run_main.py
```

_Example:_ `python run main.p --z_dim 16 --epochs 10000

### Arguments

_Optional:_

- `--z_dim`: Dimension of latent vector(z). _Default:_ `2`
- `--epochs`: The number of epochs to run. _Default:_ `100`
- `--batch_size`: The size of batch. _Default:_ `1000`
- `--lr`: Learning rate of Adam optimizer. _Default:_ `1e-4`

## References
[1] https://github.com/hwalsuklee/tensorflow-mnist-VAE  
[2] https://www.slideshare.net/NaverEngineering/ss-96581209  
[3] https://www.tensorflow.org/alpha/tutorials/generative/cvae  
